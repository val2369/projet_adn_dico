def search_one_word(searched_word,proteome,dictionnary,warning=True):
    if searched_word not in dictionnary:
        mylist=[]
        for i in searched_word:
            mylist.append(str.upper(i))
        if mylist in dictionnary:
            searched_word=mylist
        else:
            searched_word=mylist
            if warning:  
                print("This word is not in the dictionnary, but we will try to find it in the proteome anyway")
    found_number=0
    for i in range(0,len(proteome)): #for each letter of the proteome (starting at position 0)
        if proteome[i]==searched_word[0]:#if the letter we have in the proteome correspond to the first 
            #letter of the word we are interreted in, we start the scanning process of the word
            scanned_word=[] #objet to put the word being writed.
            continue_scan=True # condition to continue to scan an incomplete word
            searched_letter_position=0 #posititon in the word we are scanning
            while continue_scan : #while the word is not complete
                if i+searched_letter_position==len(proteome): 
                    #if we are scanning a word while arriving at the end of the document
                    break #we break the while loop to not bug the next if condition
                if proteome[i+searched_letter_position]==searched_word[searched_letter_position]:
                    #if the letter in the proteome is the one we search in the word,
                    scanned_word.append(proteome[i+searched_letter_position]) #we write it in the scanned word
                    if scanned_word==searched_word: #then we check if the scanned word correspond to the searched word
                        found_number+=1 #we add 1 to the number of time we found the word in the proteome.
                        continue_scan=False #close the while loop to move on in the proteome
                    else:
                        searched_letter_position+=1 #we search for the next letter
                        #and move on in the proteome
                else: #if the letter is not the one we search in the word
                    continue_scan=False # we close the while loop
                    
    return found_number
 
 
def create_dico(filetxt) :
    dicoeng=open(filetxt)
    dico_vec=[]
    for i in dicoeng.read() :
         dico_vec.append(i.upper())      #vector with all letters and separators, in capital letters.
    word_list=[[]] #the dictionnary
    word_position=0
    for i in range(0,len(dico_vec)) : #we go through dico_vec
        if dico_vec[i]=="\n" : #if it contains \n 
            word_position+=1  #we move to the next position in the list
            word_list.append([])#and start a new word in the next position
        else : #if no \n
            word_list[word_position].append(dico_vec[i]) #we continue to write the word in the position.
    return(word_list)

def search_proteome(searched_word,proteome):
    dictionnary=sow.create_dico("english-words.txt")
    foundinprot=0
    for prot in proteome: #for each protein
        foundinprot+=sow.search_one_word(searched_word,prot,dictionnary,warning=False)
        #we count the number of time the word is present and add it to the sum of previous proteins
    return(foundinprot)