# Projet_ADN_Dico

To read this project, please start with "ICEgenome3proteome_concatene" that explain the idea behind the function that search for a word.
(ICEgenome 2 and 1 are previous versions)
The final function + the function that create the dictionnary are stored in "searched_one_word.py"

Then ICEgenome_proteome use those functions and
apply them to the real proteome (it doesn't read words that are partly on one proteine and partly on another)

The new function is stored with the two others in "searched_one_word2.py"

The second part of ICEgenome_proteome search for all dictionnary word in the whole proteome, then plot the mean number fo findings as a function of the length of the words.


NOTE : I removed the print lines from readFasta in readFastaperso because they where quite bothering.