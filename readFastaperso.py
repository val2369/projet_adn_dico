# -*- coding: utf-8 -*-


# readFastaFile1
# PARAMETERS : the filename (the file is in fasta format)
# RETURNS : a unique string with all the protein sequences concatenated

def readFastaFile1(filename) :
    
    # opening the file whose name is filename
    fd = open(filename,'r')
    txt = fd.read()
    fd.close()
    
    # txt contains all the text of the file. 
    # fisrt, I want to seperate the proteins, the symbol that starts a new protein is '>'
    seqs = txt.split('>')[1:]
    s = ""
    
    for seq in seqs :
        lines = seq.split('\n')[1:]     
        for line in lines :
            s = s + line
    return(s)


# readFastaFile2
# PARAMETERS : the filename (the file is in fasta format)
# RETURNS : a list with all the protein sequences

def readFastaFile2(filename) :
    
    # opening the file whose name is filename
    fd = open(filename,'r')
    txt = fd.read()
    fd.close()
    
    # txt contains all the text of the file. 
    # fisrt, I want to seperate the proteins, the symbol that starts a new protein is '>'
    seqs = txt.split('>')[1:]
    listSeq = []
    
    for seq in seqs :
        lines = seq.split('\n')[1:]       
        s = ""  
        for line in lines :
            s = s + line
        listSeq.append(s)
    
    return(listSeq)